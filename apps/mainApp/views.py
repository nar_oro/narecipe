from django.shortcuts import render, redirect
from .models import *
from django.contrib import messages
import bcrypt


#  Dashboard -----------------------------------------------
def dashboard(request):
    if not request.session._session:
        context={
            "allCat": Categories.objects.all(),
            "allRecipies" : Recipes.objects.all(),
            "user": "Guset"
        }
    else:
        allRecipies= Recipes.objects.all()
        for r in allRecipies:
            r.myFavorite=False
            for l in r.likes.all():
                if l.id == request.session["user_id"]:
                    r.myFavorite=True
                    r.save()
        context={
            "allCat": Categories.objects.all(),
            "allRecipies" : allRecipies,
            "user": Users.objects.get(id=request.session["user_id"])
        }
    return render (request, "mainApp/dashboard.html",context)


# Post a recipe---------------------------------------------
def submitRecipe(request):
    p=request.POST
    valid=True

    if len(p["recipe_name"])<3:
        messages.error ( request, '- The recipe\'s name length must be at least 4 characters!')
        valid=False
    if int(p["prep_time"])<1:
        messages.error ( request, '- Prepration time is required!')
        valid=False
    # if int(p["cook_time"])<1:
    #     messages.error ( request, 'Cooking time is required!')
    #     valid=False
    if len(p["recipe_img"])<1:
        messages.error ( request, '- URL for image is required!')
        valid=False
    if len(p["ingred1"])<1:
        messages.error ( request, '- At least one ingredient is required!')
        valid=False
    if len(p["direction"])<10:
        messages.error ( request, '- The recipe\'s direction length must be at least 10 characters!')
        valid=False
    
    if not valid:
        return redirect ("/dashboard")
    else:
        print(p["ingred1"],p["ingred2"],p["ingred3"],p["ingred4"],p["ingred5"])
        Recipes.objects.create(
            ingred1=p["ingred1"],
            ingred2=p["ingred2"],
            ingred3=p["ingred3"],
            ingred4=p["ingred4"],
            ingred5=p["ingred5"],
            ingred6=p["ingred6"],
            ingred7=p["ingred7"],
            ingred8=p["ingred8"],
            ingred9=p["ingred9"],
            ingred10=p["ingred10"],
            name=p["recipe_name"],
            cookTime=p["cook_time"],
            prepTime=p["prep_time"],
            img=p["recipe_img"],
            direction=p["direction"],
            category=Categories.objects.get(id=p["recCat"]),
            user=Users.objects.get(id=request.session["user_id"]),
            )
    print("recipes category is:", p["recCat"])
    return redirect("/dashboard")


# User profile --------------------------------------------
def profile(request,id):
    user=Users.objects.get(id=id)
    if user == request.session["user_id"]:
        context={
        "mainUser": Users.objects.get(id=int(request.session["user_id"])),
        "userInfo":Users.objects.get(id=id),
        "user_recipes":Recipes.objects.filter(user_id=id),
        "title":"Recipes"
    }
    else:
        allRecipies= Recipes.objects.filter(user_id=id)
        for r in allRecipies:
            r.myFavorite=False
            for l in r.likes.all():
                if l.id == request.session["user_id"]:
                    r.myFavorite=True
                    r.save()
        context={
            "mainUser": Users.objects.get(id=int(request.session["user_id"])),
            "userInfo":Users.objects.get(id=id),
            "user_recipes" : allRecipies,
            "title":"Recipes"
        }
    return render (request,"mainApp/userProfile.html",context)


# Update user account form-----------------------------------
def userAccount(request,id):
    context={
        "userInfo":Users.objects.get(id=id)
    }
    return render(request,"mainApp/Account.html",context)


# Update user account-------------------------------
def updateAccount(request):
    p=request.POST
    user_id=int(p["user_id"])
    
    user=Users.objects.get(id=user_id)
    valid=True

    if len(p["first_name"]) < 2:
        messages.error ( request, '- First name length must be at least 2 characters!')
        valid=False
    if len(p["last_name"]) < 2:
        messages.error ( request, '- Last name length must be at least 2 characters!')
        valid=False
    if len(p["email"]) < 2:
        messages.error ( request, '- E-mail length must be at least 2 characters!')
        valid=False
    if len(p["password"]) < 9:
        messages.error(request, "- Password length must be at least 9 charachters.")
        valid=False
    if not ( p["password"] == p["password_confirmation"]):
        messages.error (request, "- Password and password confirmation do not match.")
        valid=False
    
    if not valid:
        return redirect ("/userAccount/"+str(user_id))
    else:
        hashed_pass=bcrypt.hashpw(p["password"].encode(), bcrypt.gensalt())
        user.first_name=p["first_name"]
        user.last_name=p["last_name"]
        user.email=p["email"]
        user.password= hashed_pass
        user.save()
        return redirect ("/dashboard")


# Update the post form----------------------------------------
def updateRecipeForm(request,id):
    recipe=Recipes.objects.get(id=id)
    context={
        "allCat": Categories.objects.all().exclude(id=recipe.category_id),
        "recipeInfo":Recipes.objects.get(id=id),
        "user": Users.objects.get(id=request.session["user_id"])
    }
    return render(request,"mainApp/updateRecipe.html",context)

    # Update the post---------------------------------
def updateRecipe(request):
    p=request.POST
    recipe_id=int(p["recipe_id"])
    recipe=Recipes.objects.get(id=recipe_id)
    valid=True

    if len(p["recipe_name"])<3:
        messages.error ( request, '- The recipe\'s name length must be at least 4 characters!')
        valid=False
    if int(p["prep_time"])<1:
        messages.error ( request, '- Prepration time is required!')
        valid=False
    # if int(p["cook_time"])<1:
    #     messages.error ( request, 'Cooking time is required!')
    #     valid=False
    if len(p["recipe_img"])<1:
        messages.error ( request, '- URL for image is required!')
        valid=False
    if len(p["ingred1"])<1:
        messages.error ( request, '- At least one ingredient is required!')
        valid=False
    if len(p["direction"])<10:
        messages.error ( request, '- The recipe\'s direction length must be at least 10 characters!')
        valid=False
    if not valid:
        return redirect ("/updateRecipeForm/"+str(recipe_id))
    else:
        recipe.ingred1=p["ingred1"]
        recipe.ingred2=p["ingred2"]
        recipe.ingred3=p["ingred3"]
        recipe.ingred4=p["ingred4"]
        recipe.ingred5=p["ingred5"]
        recipe.ingred6=p["ingred6"]
        recipe.ingred7=p["ingred7"]
        recipe.ingred8=p["ingred8"]
        recipe.ingred9=p["ingred9"]
        recipe.ingred10=p["ingred10"]
        recipe.name=p["recipe_name"]
        recipe.cookTime=p["cook_time"]
        recipe.prepTime=p["prep_time"]
        recipe.img=p["recipe_img"]
        recipe.direction=p["direction"]
        recipe.category=Categories.objects.get(id=int(p["recCat"]))
        recipe.user=Users.objects.get(id=request.session["user_id"])
        recipe.save()

        print("recipe has been updated")
    return redirect("/user/"+str(recipe.user_id))


# Recipes Info-------------------------------------------
def recipeInfo(request,id):
    if not request.session._session:
        return redirect("/loginreg")
    else:
        reviews=Reviews.objects.filter(recipe_id=id)
        recipes=Recipes.objects.get(id=id)
        ingredients=[]
        ingredients.append(recipes.ingred1)
        ingredients.append(recipes.ingred2)
        if(len(recipes.ingred3)>0):
            ingredients.append(recipes.ingred3)
            if(len(recipes.ingred4)>0):
                ingredients.append(recipes.ingred4)
                if(len(recipes.ingred5)>0):
                    ingredients.append(recipes.ingred5)
                    if(len(recipes.ingred6)>0):
                        ingredients.append(recipes.ingred6)
                        if(len(recipes.ingred7)>0):
                            ingredients.append(recipes.ingred7)
                            if(len(recipes.ingred8)>0):
                                ingredients.append(recipes.ingred8)
                                if(len(recipes.ingred9)>0):
                                    ingredients.append(recipes.ingred9)
                                    if(len(recipes.ingred10)>0):
                                        ingredients.append(recipes.ingred10)
        print(ingredients)
        context={   
            "user": Users.objects.get(id=request.session["user_id"]),
            "reviews":Reviews.objects.filter(recipe_id=id),
            "recipe":Recipes.objects.get(id=id),
            "ingred":ingredients
        }
        return render(request,"mainApp/recipeInfo.html",context)


# Create a review for recipe---------------------------------
def submitReview(request,id):
    p=request.POST
    recipe=Recipes.objects.get(id=id)
    user=Users.objects.get(id=request.session["user_id"])
    valid=True
    print(recipe.name)
    print(user.first_name)

    if len(p["review"])<3:
        # messages.error ( request, 'The recipe\'s review must be at least 4 characters!')
        valid=False
    
    if not valid:
        return redirect ("/recipe/"+str(recipe.id))
    else:
        Reviews.objects.create(
            text=p["review"],
            user=user,
            recipe=recipe,
        )
        print("doneeeeee")
        return redirect ("/recipe/"+str(recipe.id))

# Delete the post-----------------------------------------
def deleteRecipe(request,id):
    theRecipe=Recipes.objects.get(id=id)
    Recipes.objects.get(id=id).delete()
    return redirect ("/user/"+ str(theRecipe.user_id))

# Like the post-------------------------------------------
def likeA(request,id):
    user=Users.objects.get(id=int(request.session["user_id"]))
    theRecipe=Recipes.objects.get(id=id)
    theRecipe.likes.add(user)
    return redirect ("/dashboard")

# Like the post-------------------------------------------
def likeB(request,id):
    user=Users.objects.get(id=int(request.session["user_id"]))
    theRecipe=Recipes.objects.get(id=id)
    theRecipe.likes.add(user)
    return redirect ("/user/"+ str(user.id)+"/favorite")

# Delete the like-----------------------------------------
def dislikeA(request,id):
    user=Users.objects.get(id=int(request.session["user_id"]))
    theRecipe=Recipes.objects.get(id=id)
    theRecipe.likes.remove(user)
    return redirect ("/dashboard")

# Delete the like-----------------------------------------
def dislikeB(request,id):
    user=Users.objects.get(id=int(request.session["user_id"]))
    theRecipe=Recipes.objects.get(id=id)
    theRecipe.likes.remove(user)
    return redirect ("/user/"+ str(user.id)+"/favorite")


# Favorite Recipes ---------------------------------------
def favoriteRecipes(request,id):
    user=Users.objects.get(id=id)
    favoriteRecipes=[]
    allRecipies= Recipes.objects.all()
    for r in allRecipies:
        for l in r.likes.all():
            if l.id == user.id:
                favoriteRecipes.append(r)

    if user == request.session["user_id"]:       
        context={
            "mainUser": Users.objects.get(id=int(request.session["user_id"])),
            "userInfo":Users.objects.get(id=id),
            "user_recipes":favoriteRecipes,
            "title":"Favorite Recipes"
        }
    else:
        for f in favoriteRecipes:
            f.myFavorite=False
            for l in f.likes.all():
                if l.id == request.session["user_id"]:
                    r.myFavorite=True
                    r.save()
        context={
            "mainUser": Users.objects.get(id=int(request.session["user_id"])),
            "userInfo":Users.objects.get(id=id),
            "user_recipes":favoriteRecipes,
            "title":"Favorite Recipes"
        }
    return render (request,"mainApp/userProfile.html",context)

# Create a category---------------------------------------
def createCategory(request):
    p=request.POST

    Categories.objects.create(name=p["cat"])
    print("New category name is: ", p["cat"])
    return redirect ("/dashboard")