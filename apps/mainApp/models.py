from django.db import models
from ..loginReg.models import *


class Categories(models.Model):
    name= models.CharField(max_length=100)

    created_at=models.DateTimeField(auto_now_add=True)
    updated_at=models.DateTimeField(auto_now=True)


class Recipes(models.Model):
    ingred1= models.CharField(max_length=30)
    ingred2= models.CharField(max_length=30)
    ingred3= models.CharField(max_length=30)
    ingred4= models.CharField(max_length=30)
    ingred5= models.CharField(max_length=30)
    ingred6= models.CharField(max_length=30)
    ingred7= models.CharField(max_length=30)
    ingred8= models.CharField(max_length=30)
    ingred9= models.CharField(max_length=30)
    ingred10= models.CharField(max_length=30)
    
    name=models.CharField(max_length=100)
    prepTime=models.IntegerField()
    cookTime=models.IntegerField()
    img=models.CharField(max_length=100)
    direction= models.CharField(max_length=300)

    user = models.ForeignKey( Users, related_name="user_recipes")
    category= models.ForeignKey(Categories, related_name="recipes_category", default="0")
    likes = models.ManyToManyField(Users, related_name="liked_recipes")
    myFavorite=models.BooleanField(default=False)
    
    created_at=models.DateTimeField(auto_now_add=True)
    updated_at=models.DateTimeField(auto_now=True)


class Reviews(models.Model):
    text=models.CharField(max_length=300)
    user = models.ForeignKey( Users, related_name="user_reviews")
    recipe = models.ForeignKey( Recipes, related_name="recipe_reviews")

    created_at=models.DateTimeField(auto_now_add=True)
    updated_at=models.DateTimeField(auto_now=True)