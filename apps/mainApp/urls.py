from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^dashboard$',views.dashboard),
    url(r'^submitRecipe$',views.submitRecipe),

    url(r'^user/(?P<id>\d+)$',views.profile),
    url(r'^user/(?P<id>\d+)/favorite$',views.favoriteRecipes),
    url(r'^userAccount/(?P<id>\d+)$',views.userAccount),
    url(r'^userAccount/updateAccount',views.updateAccount),

    url(r'^updateRecipeForm/(?P<id>\d+)$',views.updateRecipeForm),
    url(r'^updateRecipeForm/updateRecipe$',views.updateRecipe),

    url(r'^recipe/(?P<id>\d+)$',views.recipeInfo),
    url(r'^deleteRecipe/(?P<id>\d+)$',views.deleteRecipe),
    url(r'^Like/(?P<id>\d+)$',views.likeA),
    url(r'^user/like/(?P<id>\d+)$',views.likeB),
    url(r'^dislike/(?P<id>\d+)$',views.dislikeA),
    url(r'^user/dislike/(?P<id>\d+)$',views.dislikeB),
    url(r'^recipe/submitReview/(?P<id>\d+)$',views.submitReview),


    url(r'^submitCategory$',views.createCategory),

]