from django.shortcuts import render, redirect
from django.contrib import messages
from .models import Users
import bcrypt


# Home Page--------------------------------------------------
def home(request):
    return render(request,"loginReg/home.html")


def loginReg(request):
    return render(request,"loginReg/loginReg.html")

# registaration-------------------------------------------------------------------
def register(request):
    p= request.POST
    request.session["first_name"]= p["first_name"]
    valid=True
  

    if len(p["first_name"]) < 2:
        messages.error(request, "- First name length must be at least 2 charachters!")
        valid=False
    if len(p["last_name"]) < 1:
        messages.error(request, "- Last name length must be at least 1 charachter!")
        valid=False
    if len(p["email"]) < 2:
        messages.error(request,"- Email length must be at least 2 charachters!")
        valid=False
    if len(p["password"]) < 9:
        messages.error(request, "- Password length must be at least 9 charachters!")
        valid=False

    if not ( p["password"] == p["password_confirmation"]):
        messages.error (request, "- Password and password confirmation do not match!")
        valid=False

    if not valid:
        return redirect("/loginreg")
    else:
        if Users.objects.filter(email= p["email"]).exists():
            messages.error(request, "- You are already registered , please login!")
            return redirect("/loginreg")
        else:
            hashed_pass=bcrypt.hashpw(p["password"].encode(), bcrypt.gensalt())
            Users.objects.create(first_name = p["first_name"], last_name = p["last_name"], email = p["email"], password = hashed_pass)
            messages.error(request, "- Congratulation you successfully registered, please login now!")
            return redirect("/loginreg")

# login----------------------------------------------------------------------------------
def login(request):
    p= request.POST
    print(p)
    try:
        print("try")
        user= Users.objects.get(email = p["email"])
        print(user.email)
        print(user.password)
        password_valid= bcrypt.checkpw(p["password"].encode(), user.password.encode())
        print(password_valid)
        if password_valid:
            request.session["logged_in"] = True
            request.session["user_id"] = user.id
            request.session["first_name"] = user.first_name
            return redirect("/dashboard")
        else:
            messages.error(request, "- Password/email did not match")
    
    except Users.DoesNotExist:
        messages.error(request, "- Could not find user with that email")

    except:
        messages.error(request, "- Something else went Wrong.")
    return redirect("/loginreg")

# logout-------------------------------------------------------------------
def logout(request):
    request.session.clear()
    return redirect("/")
# --------------------------------------------------------------------------









