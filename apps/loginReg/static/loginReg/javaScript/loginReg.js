

if ($(window).width()> 768){

    $(document).ready(function(){
        $("#register-btn").click(function(){
            $("#main").animate({left:"22.5%"},400);
            $("#main").animate({left:"30%"},300);
     
            $(".loginForm").css("visibility", "hidden");
            $(".loginForm").animate({left:"25%"}, 400);
     
            $(".registerForm").css("visibility", "visible");
            $(".registerForm").animate({left:"17%"},400);
            $(".registerForm").animate({left:"30%"},300);
     
            
         });
         
         $("#login-btn").click(function(){
             $("#main").animate({left:"77.5%"},400);
             $("#main").animate({left:"70%"},300);
     
             $(".loginForm").css("visibility", "visible");
             $(".loginForm").animate({left:"83.5%"},400);
             $(".loginForm").animate({left:"70%"},300);
     
             $(".registerForm").css("visibility", "hidden");
             $(".registerForm").animate({left:"75%"}, 400);
        });
     });

}else if($(window).width() == 768){
    $(document).ready(function(){
        $("#register-btn").click(function(){
            $("#main").animate({left:"22.5%"},400);
            $("#main").animate({left:"30%"},300);
     
            $(".loginForm").css("visibility", "hidden");
            $(".loginForm").animate({left:"25%"}, 400);
     
            $(".registerForm").css("visibility", "visible");
            $(".registerForm").animate({left:"17%"},400);
            $(".registerForm").animate({left:"30%"},300);
     
            
         });
         
         $("#login-btn").click(function(){
             $("#main").animate({left:"77.5%"},400);
             $("#main").animate({left:"70%"},300);
     
             $(".loginForm").css("visibility", "visible");
             $(".loginForm").animate({left:"83.5%"},400);
             $(".loginForm").animate({left:"70%"},300);
     
             $(".registerForm").css("visibility", "hidden");
             $(".registerForm").animate({left:"75%"}, 400);
        });
     });
}